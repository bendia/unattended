    #!/bin/bash
    ###################
    # Basé sur https://debian-facile.org/git/ProjetsDF/dfiso-buster/src/branch/master/config/includes.chroot/usr/share/dflinux/welcome.sh
    # Sur une proposition de Benoît 

    # message d'accueil
    zenity --title "Bienvenue" --info --ok-label "Bonne découverte et @+" --ellipsize --text "<b>Bonjour et bienvenue ☺</b>

    Cet ordinateur a été configuré par l'association LINUXMAINE.
    Notre association fait la promotion des logiciels libres.
    Cet ordinateur est fourni avec le système d'exploitation XUBUNTU de GNU/Linux.
    
    Un système d'exploitation libre possède de nombreux avantages:
    . Il utilise des applications modernes et à jour.
    . La licence des logiciels libres est gratuite.
    . Un ordinateur equipé de GNU/Linux n'est pas sensible aux virus (si les règles de base de sécurité sont respectées)
    . Vous avez accès à des milliers d'applications gratuitement (bureautique, internet, dessin, audio, jeux, etc).
          
    N'hésitez pas à visiter <a href=\"https://linuxmaine.org/spip.php?article58\">la page du projet</a> 
    afin de nous faire part de vos retours et suggestions.

    Merci à tous ceux qui ont participé au projet de dons pour la continuité pédagogique pendant le confinement.
    "

    # déplacement du lanceur de l'écran d'accueil sur le bureau
    if [ -e /home/$USER/.config/autostart/welcome.desktop ]; then
        mv /home/$USER/.config/autostart/welcome.desktop /home/$USER/Bureau
    fi

    exit 0
