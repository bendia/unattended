#!/bin/bash

# vérification sudo
if [ $EUID -ne 0 ]
then
        echo "Vous devez lancer ce script avec des privilèges root (sudo bash pinstall.sh)"
        exit 1
else
        echo "Script exécuté avec les privilèges root"
fi

# le proxy est utilisé est utilisé uniquement au local linuxmaine
# echo "Acquire::http::Proxy \"http://192.168.1.99:3142\";" >  /etc/apt/apt.conf.d/proxy

# mise à jour 
apt -y update
apt -y upgrade

# désinstallation de abiword et gnumeric
apt -y purge abiword
apt -y purge gnumeric

# installation de synaptic
apt install -y synaptic

# installation du lecteur multimedia
apt-get -y install vlc

# installation du logiciel d'inventaire
export DEBIAN_FRONTEND=noninteractive
apt-get -q -y install ocsinventory-agent
echo "server=local.linuxmaine.org/ocsinventory" > /etc/ocsinventory/ocsinventory-agent.cfg
ocsinventory-agent

# nettoyage 
apt-get autoremove

# 
#rm  /etc/apt/apt.conf.d/proxy
#rm /etc/apt/apt.conf

#cat /etc/network/interfaces

#raccourcis libreoffice sur le bureau
echo -e  "[Desktop Entry]\nTerminal=false\nIcon=libreoffice-calc\nType=Application\nExec=libreoffice --calc\nName[fr]=LibreOffice Calc" > ~/Bureau/libreoffice-calc.desktop 
echo -e  "[Desktop Entry]\nTerminal=false\nIcon=libreoffice-writer\nType=Application\nExec=libreoffice --writer\nName[fr]=LibreOffice Writer" > ~/Bureau/libreoffice-writer.desktop 
chmod 755 ~/Bureau/libreoffice-calc.desktop
chmod 755 ~/Bureau/libreoffice-writer.desktop
if ! [ -e /home/linuxmaine/.config/autostart/welcome.desktop ]; then
	mkdir -p /home/linuxmaine/.config/autostart
        wget -O /home/linuxmaine/.config/autostart/welcome.desktop "https://framagit.org/linuxmaine/unattended/-/raw/master/welcome.desktop"
        chmod 755 /home/linuxmaine/.config/autostart/welcome.desktop
	chown -R linuxmaine:linuxmaine /home/linuxmaine/.config/autostart
fi	

if ! [ -e /usr/share/linuxmaine/welcome.sh]; then
	mkdir -p /usr/share/linuxmaine
        wget -O /usr/share/linuxmaine/welcome.sh "https://framagit.org/linuxmaine/unattended/-/raw/master/welcome.sh"
        chmod 755 /usr/share/linuxmaine/welcome.sh
fi

#copier le lanceur de firefox sur le bureau
cp /usr/share/applications/firefox.desktop /home/linuxmaine/Bureau
chmod 755 /home/linuxmaine/Bureau/firefox.desktop
