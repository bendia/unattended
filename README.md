Check-list de préparation standard avant livraison

Après ouverture, mise à niveau des composants matériels (DD, RAM) et nettoyage (enlèvement des sigles windows):

1- Installer Xubuntu 18.04 LTS ( ! portable avec carte graphique
     Nvidia par ex, installer le pilote) – si tu sais faire, tu peux utiliser le script de Nordine (pinstall.sh – voir le lien sur le framapad- en faisant quand même les tests préconisés ci-dessous), sinon :
     
2- Faire les mises à jour (update-upgrade)

3- Installer si pas inclus « synaptic »

4- Installer si pas inclus « VLC »

5- Tester le son (internet +CD)

6- Tester lecture/écriture clé USB

7- Mettre sur le bureau les icônes « writer » et « Calc »

8- Vérifier si mise en veille et réveil fonctionnent (si Pb : voir soit dans « paramètres/gestionnaire d'alimentation » (supprimer gestion d'écran+bouton alim programmé sur « éteindre »), soit dans le BIOS (power management changer S1 en S3 ou réciproquement).

9- Tester la fonction « multi-espace de travail » en vérifiant si l’icône s’affiche bien dans la barre supérieure (menu/paramètres/ espace de travail) : monter à 2 puis vérifier si l’icône s’affiche bien dans la barre. Si ce n’est pas le cas : clic droit sur barre supérieure, puis »tableau de bord/ajouter de nouveaux éléments/changeur d’espace de travail/ajouter ».

10- Mettre un fond d’écran adapté : paramètres/bureau (les photos sont dans sys. De fichiers/USR/Share/xfce4).

11- Le login d’accueil sera « linuxmaine » ainsi que le mot de passe. Quand Nordine en auras fini la rédaction, il faudra coller un fichier « lisez-moi » sur le bureau (explication du don, etc …).

12- Préparer (nettoyer) un écran, une souris et un clavier.
